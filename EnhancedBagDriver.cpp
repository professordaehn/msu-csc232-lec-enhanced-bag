#include <cstdlib>
#include <iostream>
#include <string>
#include "BagInterface.h"
#include "ArrayBag.h"

int main(int argc, char** argv) {
	std::cout << "Hi there..." << std::endl;

	BagInterface<std::string> *thisBag = new ArrayBag<std::string>();
	BagInterface<std::string> *thatBag = new ArrayBag<std::string>();
	BagInterface<std::string> *myBag;

	myBag = thisBag->getUnionWithBag(thatBag);
	std::cout << "myBag->isEmpty() = " << (myBag->isEmpty() ? "true" : "false") << std::endl;
	return EXIT_SUCCESS;
}
